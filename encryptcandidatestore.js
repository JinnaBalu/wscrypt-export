const fs = require('fs');

module.exports = function (app, utilities, env, encryptionUtil) {
    const sendData = utilities.sendData;
    const encrypt = encryptionUtil.encrypt;
    const decrypt = encryptionUtil.decrypt;

    fs.existsSync("candidatestorescript.cql") && fs.unlinkSync("candidatestorescript.cql")
    var stream = fs.createWriteStream("candidatestorescript.cql");

    var context = {
        _scroll_id: null,
        index: 0,
        response: null,
    }

    function start() {
        console.log("Process Started");
        context.index = 0;
        createCandidateScrollToken();
    }

    function createCandidateScrollToken() {
        sendData({
            "size": 10000,
            "_source": {
                "include": ["id", "phoneNumber"]
            }
        }, utilities.getCandidateIndex() + "candidatestore/_search?scroll=2m", "POST", {
                scroll: "2m"
            }).then(function (response) {
                console.log(response);
                context.response = response;
                writeToFile();
            }).catch(function (err) {
                console.log(err);
            });
    }

    function writeToFile() {
        console.log(context.response);
        if (context.response.hits.hits.length > 0) {
            context.index = 0;
            context._scroll_id = context.response._scroll_id;
            encrptCandidateAndWriteToFile();
        } else {
            console.log("Process is completed")
        }
    }

    function encrptCandidateAndWriteToFile() {
        if (context.response.hits.hits[context.index]) {
            var candidate = context.response.hits.hits[context.index]["_source"];
            var data = [];
            if (candidate.phoneNumber && candidate.phoneNumber.trim().length > 0) {
                data.push("phoneNumber='" + encryptionUtil.encrypt(candidate.phoneNumber) + "'");
                stream.write("UPDATE candidate SET " + data.join(",") + " WHERE id = " + candidate.id + ";\n");
                context.index = context.index + 1;
                setTimeout(encrptCandidateAndWriteToFile, 10);
            } else {
                context.index = context.index + 1;
                setTimeout(encrptCandidateAndWriteToFile, 10);
            }
        } else {
            scrollToCandidateNextPage();
        }
    }

    function scrollToCandidateNextPage() {
        sendData({
            "scroll": "2m",
            "scroll_id": context._scroll_id
        }, utilities.getCandidateIndex() + "/_search/scroll", "POST").then(function (response) {
            context.response = response;
            writeToFile();
        }).catch(function (err) {
            console.log(err);
        });;
    }

    setTimeout(start, 5000);
}
