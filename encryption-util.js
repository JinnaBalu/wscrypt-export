"use strict";
var crypto = require("crypto");
var env;
const key = "DCDD74627CD60252E35DFBA91A4556AA";
var iv = new Buffer("53IQ1tPX3aHxzqV4");
    
module.exports = {
    init: function (e) {
        env = e;
    },
    decrypt: function(data){
        var encodeKey = crypto.createHash('sha256').update(key, 'utf-8').digest();
        var cipher = crypto.createDecipheriv('aes-128-cbc', new Buffer(key, 'hex'), new Buffer(iv));
        return cipher.update(data, 'hex', 'utf8') + cipher.final('utf8');
    },
    encrypt: function(data) {
        var encodeKey = crypto.createHash("sha256").update(key, "hex").digest();
        var cipher = crypto.createCipheriv("aes-128-cbc", new Buffer(key, "hex"), new Buffer(iv));
        return cipher.update(data, "utf8", "hex") + cipher.final("hex");
    },

};

// var EncryptionHelper = (function () {
//     const key = "DCDD74627CD60252E35DFBA91A4556AA";
//     // key examples: 
//     // DCDD74627CD60252E35DFBA91A4556AA
//     // 2CB24CFDB3F2520A5809EB4851168162
//     // 468CA14CA44C82B8264F61D42E0E9FA1
//     var iv = new Buffer("53IQ1tPX3aHxzqV4");
//     function decrypt(data) {
//         var encodeKey = crypto.createHash('sha256').update(key, 'utf-8').digest();
//         var cipher = crypto.createDecipheriv('aes-128-cbc', new Buffer(key, 'hex'), new Buffer(iv));
//         return cipher.update(data, 'hex', 'utf8') + cipher.final('utf8');
//     }

//     function encrypt(data) {
//         var encodeKey = crypto.createHash("sha256").update(key, "hex").digest();
//         var cipher = crypto.createCipheriv("aes-128-cbc", new Buffer(key, "hex"), new Buffer(iv));
//         return cipher.update(data, "utf8", "hex") + cipher.final("hex");
//     }

//     return {
//         decrypt: decrypt,
//         encrypt: encrypt
//     };

// })();

// module.exports = EncryptionHelper;