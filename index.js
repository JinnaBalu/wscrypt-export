const env = process.env.NODE_ENV ? process.env.NODE_ENV : 'development';
//Initiallising node modules
const express = require("express");
const bodyParser = require("body-parser");
const utilities = require('./utilities');
const encryptionUtil = require('./encryption-util');
//Initalise utilities
utilities.init(env);

const rp = require('request-promise');
const querystring = require('querystring');
const errorhandler = require('errorhandler')
const app = express();


//node server.js 7001 1 2
// Body Parser Middleware
app.use(bodyParser.json());

//CORS Middleware
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization");
    next();
});

var port = process.argv[2];
var type = process.argv[3];


//Setting up server
var server = app.listen(port || 7001, function () {
    var port = server.address().port;
    console.log("App now running on port :: " + port);
});

//node index.js 10001 1 0

if (type == "1")
    require('./encryptcandidate')(app, utilities, env, encryptionUtil);
else if (type == "2")
    require('./encryptcandidatestore')(app, utilities, env, encryptionUtil);

//
//require('./candidateMappings')(app, utilities);
//require('./jobteam')(app, utilities);
//require('./recruierteam')(app, utilities);
///
//
//
//
