const fs = require('fs');

module.exports = function (app, utilities, env, encryptionUtil) {
    const sendData = utilities.sendData;
    const encrypt = encryptionUtil.encrypt;
    const decrypt = encryptionUtil.decrypt;

    fs.existsSync("candidatescript.cql") && fs.unlinkSync("candidatescript.cql")
    var stream = fs.createWriteStream("candidatescript.cql");

    var context = {
        _scroll_id: null,
        index: 0,
        response: null,
    }

    function start() {
        console.log("Process Started");
        context.index = 0;
        createCandidateScrollToken();
    }

    function createCandidateScrollToken() {
        sendData({
            "size": 10000,
            "_source": {
                "include": ["id", "phoneNumber", "isJobCandidateMapping"]
            }
        }, utilities.getCandidateIndex() + "jobcandidateinteraction/jobcandidateinteraction/_search?scroll=2m", "POST", {
                scroll: "2m"
            }).then(function (response) {
                context.response = response;
                writeToFile();
            }).catch(function (err) {
                console.log(err);
            });
    }

    function writeToFile() {
        if (context.response.hits.hits.length > 0) {
            context.index = 0;
            context._scroll_id = context.response._scroll_id;
            encrptCandidateAndWriteToFile();
        } else {
            console.log("Process is completed")
        }
    }

    function encrptCandidateAndWriteToFile() {
        if (context.response.hits.hits[context.index]) {
            var candidate = context.response.hits.hits[context.index]["_source"];
            var data = [];
            if (candidate.phoneNumber && candidate.phoneNumber.trim().length > 0) {
                data.push("phoneNumber='" + encryptionUtil.encrypt(candidate.phoneNumber) + "'");
                if (candidate.isJobCandidateMapping) {
                    stream.write("UPDATE job_candidate_mapping SET " + data.join(",") + " WHERE id = " + candidate.id + ";\n");
                } else {
                    stream.write("UPDATE jobcandidateinteraction SET " + data.join(",") + " WHERE id = " + candidate.id + ";\n");
                }
                context.index = context.index + 1;
                setTimeout(encrptCandidateAndWriteToFile, 10);
            }else{
                context.index = context.index + 1;
                setTimeout(encrptCandidateAndWriteToFile, 10);
            }
        } else {
            scrollToCandidateNextPage();
        }
    }

    function scrollToCandidateNextPage() {
        sendData({
            "scroll": "2m",
            "scroll_id": context._scroll_id
        }, utilities.getCandidateIndex() + "/_search/scroll", "POST").then(function (response) {
            context.response = response;
            writeToFile();
        }).catch(function (err) {
            console.log(err);
        });;
    }

    setTimeout(start, 5000);

}
